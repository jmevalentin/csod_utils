<?php

namespace Drupal\csod_utils\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use mysql_xdevapi\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\user\Entity\User;


/**
 * Returns responses for Cornerstone OnlineDemand Utilities routes.
 */
class CornerstoneOnlineDemandUtilitiesController extends ControllerBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  protected $modulePath;
  protected $oauth_token;
  protected $user;
  /**
   * Constructs the controller object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
    $module_handler = Drupal::service('module_handler');
    $this->modulePath = $module_handler->getModule('csod_utils')->getPath();
    $this->oauth_token = $this->getOauthToken();
    $this->setUser();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter')
    );
  }

  /**
   * function csod_call()
   * @param $url
   * @param $headers
   * @param array $dataArray
   * @param string $method
   * @return false|string
   *
   * execute curl calls
   */
  private function csod_call($url, $headers, $dataArray = array(), $method = 'GET')  {

    $curl_method = ($method=='POST') ? CURLOPT_POST  : CURLOPT_HTTPGET;
    try {
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      if($method == 'POST') {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $dataArray ) );
      }
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $transactionXML = curl_exec($ch);
    } catch (Exception $e) {
      $transactionXML = $e;
      print_r($e);
    }

    $json_array = json_encode($transactionXML);

    return $json_array;
  }

  /**
   * function getOauthToken() {
   * @return false|mixed|string
   * get a fresh token
   */
  private function getOauthToken() {
    try {
      $this->oauth_token = $this->csod_call('https://roberthalf-pilot.csod.com/services/api/oauth2/token',
        array(
          'clientId' => '???',
          'clientSecret' => '???',
          'grantType' => 'client_credentials',
          'scope' => 'employee:read employee:create'
        )
      );
    } catch (Exception $e) {
      $this->oauth_token = '0';
    }
    return $this->oauth_token;
  }

  /**
   * function verify_csod_account()
   * @return bool|false|string
   * Check if the user accountr exists
   */
  private function verify_csod_account() {

    try {
      $account = $this->csod_call('https://roberthalf-pilot.csod.com/services/api/x/users/v1/employees/userid-diana.prince@kltest.com',
        array(
          'Authorization' => $this->oauth_token
        )
      );
    } catch (Exception $e) {
      $account = array(true);
    }

    return $account;
  }


  /**
   * Runs a simulated employee account check on CSOD systemp;
   * Allows for override of results to force test
   */
  public function test_verify_csod_account($teststat) {
    $existfail ='';
    $sourcefile ='';
    switch($teststat) {
      case 'success':
        $sourcefile ='sample_success.txt';
        break;
      case 'fail':
        $sourcefile = 'sample_fail.txt';
        $existfail = " does NOT ";
        $postmsg = ' Now creating an account for this user.';
        break;
      default:
        $sourcefile = $this->verify_csod_account();
    }

    $source = file_get_contents($this->modulePath . '/assets/'.$sourcefile);
    $results =  '<pre>' . print_r(($source), 1) . '</pre>';
    $output = 'This user '. $existfail .' exists in the CSOD system: <br>' .$results;

    $build['content'] =[
      '#type' => 'item',
      '#title' => $this->t('Checking Account...'),
      '#markup' => $this->t($output),
    ];

    if($teststat == 'fail'){
      $checkMsg = '<a href="/csod-utils/create-csod-account">'. t("Click here below to create the account") . '</a>';
    } else {
      $checkMsg = '<a href="/csod-utils/create-csod-account">'. t("Click here below to display the account") . '</a>';
    }

    $build['create_account'] =[
      '#type' => 'item',
      '#title' => $this->t('Create Account'),
      '#markup' => $checkMsg,
    ];

    return $build;
  }

  /*
   * Setter for $user
   * set to current user for now
   */
  public function setUser($userId = 0) {
    if($userId == 0) {
      $user = \Drupal\user\Entity\User::load(Drupal::currentUser()->id());
    } else {
      $user = \Drupal\user\Entity\User::load($userId);
    }
    $newUserData = array(
      'userId' => $user->getEmail(),
      'userName' => $user->getEmail(),
      'firstName' => 'Jamie',     //$user->get('field_first_name')->getValue()[0]['value'],
      'lastName' =>  'Valentin',  //$user->get('field_last_name')->getValue()[0]['value'],
      'primaryEmail' => $user->getEmail(),
      'address'=> array(
        "country" => "USA",
        "ous" => array(
          [ "ouId" => "5", "type" => "Organization" ], [ "ouId" => "USA",  "type" => "Location" ]
        )
      )
    );

    $this->user = $newUserData;
  }

  /*
   *  Getter for $user
   */
  public function getUser() {
    return $this->user;
  }

  /**
   * Create the account on CSOD
   */
  public function create_csod_account($confirm = 1) {
    $newUserData = $this->getUser();

    $rawdata = json_encode( $newUserData );

    $create_data = array($this->oauth_token, $rawdata);
   // dpm($newUserData);

    if($confirm <> 0) {
      //START DEMO CODE (Confirmation)
      $build['new_user_account_msg'] = [
        '#type' => 'item',
        '#title' => $this->t('Creating User Account'),
        '#markup' => t('The following user will be created:'),
      ];

      $build['new_user_account_data'] = [
        '#type' => 'item',
        '#title' => $this->t(' User Account data to be written:'),

        '#markup' => t('</pre>' . print_r($newUserData, 1) . '</pre>'),
        '#attributes' => array('styles' => ['border-width: 2px', 'border-color:red', 'border-style:dotted'])
      ];

      $build['create_confirm_link'] = [
        '#type' => 'item',
//      '#title' => $this->t(' User Account data to be written:'),
        '#markup' => t('<a href="/csod-utls/create-csod-account/0">Click here to comfirm account creation</a>'),
        '#attributes' => array('styles' => ['border-width: 2px', 'border-color:red', 'border-style:dotted'])
      ];
      //END DEMO CODE
    } else {
      try {
        $this->csod_call('https://roberthalf-pilot.csod.com/services/api/x/users/v1/employees/userid-' . $newUserData['userId'],
          array(
            'Authorization' => $this->oauth_token,
            array(),
            'POST'
          )
        );
      }
      catch (Exception $e) {
        $transactionXML = $e;
        print_r($e);
      }
    }

    return $build;

  }


  /**
   *  Goto user account on CSOD system
   */
  public function access_csod_account($userId) {
    $user = $this->getUser();
    $success = $this->csod_call('https://roberthalf-pilot.csod.com/services/api/x/users/v1/employees/userid-'.$user['userId'],
      array(
        'Authorization' => $this->oauth_token
      )
    );

    return $success;
  }


  /**
   * Runs a simulated employee account check on CSOD system with the result of Success
   */
  public function test_success() {
    $source = file_get_contents(  $this->modulePath . '/assets/sample_success.txt');
    $results = '<pre>' . print_r( ($source), 1) . '</pre>';

    $output = "This user exists in the CSOD system. Here is a summary of their account: <br>" . $results;

    $build['content'] =[
      '#type' => 'item',
      '#title' => $this->t('Checking Account...'),
      '#markup' => $this->t($output),
    ];

    return $build;
  }
  /**
   * Runs a simulated employee account check on CSOD system with the result of Fail
   */
  public function test_fail() {
    $source = file_get_contents(  $this->modulePath . '/assets/sample_fail.txt');
    $results = '<pre>' . print_r( ($source), 1) . '</pre>';

    $output = "This user does NOT exist in the CSOD system. Here is a summary of their account: <br>" . $results;

    $build['content'] =[
      '#type' => 'item',
      '#title' => $this->t('Checking Account...'),
      '#markup' => $this->t($output),
    ];

    return $build;
  }


}
