<?php

namespace Drupal\csod_utils\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Cornerstone OnlineDemand Course Access' Block.
 *
 * @Block(
 *   id = "csod_course_access_block",
 *   admin_label = @Translation("CSOD Course Access"),
 *   category = @Translation("Cornerstone OnlineDemand Utilities"),
 * )
 */
class CornerstoneodBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  /*
   * Sample params:
      curl -L -X GET 'https://roberthalf-pilot.csod.com/services/api/x/users/v1/employees/userid-diana.prince@kltest.com' \
      -H 'Cache-Control: no-cache' \
      -H 'Authorization: Bearer ZWRnZQ:5124fe765def8390d52a1cbb3de0ceb4f4a934b49f1f6666fb5f22cb1dbce6e2'
   *
   */
  public function build() {
//    $course_link = '<a href="https://roberthalf-pilot.csod.com/samldefault.aspx?ouid=2&returnurl=%252fDeepLink%252fProcessRedirect.aspx%253fmodule%253dloRegisterAndLaunch%2526lo%253d1f60d66e-80e6-4c2d-be51-431d7f968ac0">' . t(" here "). '</a>';
    $vals = array(
      $url = 'https://gist.githubusercontent.com/sritchie/1058463/raw/3fbf00adfd4e0337d4f93cecc801bd85874645cc/gist.xml',
      $headers = array(
        'Content-Type' => 'application/json',
  //      'Authorization' => 'Bearer ZWRnZQ:5124fe765def8390d52a1cbb3de0ceb4f4a934b49f1f6666fb5f22cb1dbce6e2',
      ),
    );
    $testcall = $this->csod_call($url, $headers);

    $build['#markup'] = print_r($testcall, 1);
    return $build;
  }

  function prepCSContent() {
    // 1. Get oauth2 token
    $oauth_token = $this->csod_call('https://roberthalf-pilot.csod.com/services/api/oauth2/token',
      array(
        'clientId' => 'svcLMSws1amPILOT',
        'clientSecret' => '',
        'grantType' => 'client_credentials',
        'scope' => 'employee:read employee:create'
      )
    );

    //2. Get user id (email address)
    //TODO

    //3. Check for user account in Cornerstone OD
    $csodAccountExists = $this->csod_call(
        'https://roberthalf-pilot.csod.com/services/api/x/users/v1/employees/userid-diana.prince@kltest.com',
        array('Authorization' => $oauth_token)
      );

    //4. If user doesn't have  CSOD Account, create it
    if($csodAccountExists['status'] == 'Failure') {
      $createAccount = $this->csod_call(
        'https://roberthalf-pilot.csod.com/services/api/x/users/v1/employees',
        array (
          'Content-Type' => 'application/json',
          'Authorization' => $oauth_token,
        ),
        array (
          "userId" => "juan.moretry@kltest.com",
          "userName" => "juan.moretry@kltest.com",
          "firstName" => "Juan",
          "lastName" => "Moretry",
          "primaryEmail" => "juan.moretry@kltest.com",
          "address" => array(
            "country" => "USA", "ous" => array(
              ["ouId"=> "5", "type" => "Organization"],
              ["ouId" => "USA", "type" => "Location"]
            ),
        ),
          'POST'
        )
      );
    }

  }

  /*
   *  Returns JSON array
   */
  public function csod_call($url, $headers, $dataArray = array(), $method = 'GET') {

    $curl_method = ($method=='POST') ? CURLOPT_POST  : CURLOPT_GET;
    try {
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      if($method == 'POST') {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $dataArray ) );
      }
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $transactionXML = curl_exec($ch);
    } catch (Exception $e) {
      $transactionXML = $e;
      print_r($e);
    }
//    $xml = simplexml_load_string($transactionXML);
//    $json = json_encode($xml);
//    $json_array = json_decode($transactionXML, TRUE);
    $json_array = json_encode($transactionXML);

    return $json_array;
  }


//
//  public function csod_call2($method, $url, $data = false) {
//
//    $curl = curl_init();
//
//    switch ($method)
//    {
//      case "POST":
//        curl_setopt($curl, CURLOPT_POST, 0);
//
//        if ($data)
//          curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
//          curl_setop($curl, CURLOPT_HEADER, TRUE);
//        break;
//      case "GET":
//        curl_setopt($curl, CURLOPT_GET, 0);
//
//        if ($data)
//          curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
//        break;
//      case "PUT":
//        curl_setopt($curl, CURLOPT_PUT, 1);
//        break;
//      default:
//        if ($data)
//          $url = sprintf("%s?%s", $url, http_build_query($data));
//    }
//
//    // Optional Authentication:
//    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//    curl_setopt($curl, CURLOPT_USERPWD, "username:password");
//
//    curl_setopt($curl, CURLOPT_URL, $url);
//    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//    $result = curl_exec($curl);
//
//    curl_close($curl);
//
//    return $result;
//  }

}
